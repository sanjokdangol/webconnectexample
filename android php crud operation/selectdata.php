<?php
include_once 'dbconnect.php';

$sql = "Select * from users ";

$result = mysqli_query($conn, $sql);
$array = array();

while ($row=mysqli_fetch_assoc($result)) {
	$array[]=$row;
}

header('Content-Type: application/json');
echo json_encode($array);
