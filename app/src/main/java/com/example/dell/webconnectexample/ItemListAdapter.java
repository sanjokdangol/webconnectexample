package com.example.dell.webconnectexample;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Dell on 14/07/2017.
 */

public class ItemListAdapter extends BaseAdapter {
LayoutInflater inflater;
    Context c;
    ArrayList<HashMap<String,String>> items;
    ItemListAdapter(Context context, ArrayList<HashMap<String,String>> result){
        this.items=result;
        this.c=context;
        this.inflater=LayoutInflater.from(c);
    }
    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder=new ViewHolder();
        if(convertView==null){
            convertView=inflater.inflate(R.layout.list_item,null);
          //  holder.imageView= (ImageView) convertView.findViewById(R.id.image);
            holder.name= (TextView) convertView.findViewById(R.id.name);
            holder.email= (TextView) convertView.findViewById(R.id.email);
            holder.mobile= (TextView) convertView.findViewById(R.id.mobile);
            convertView.setTag(holder);
        }else {
            holder= (ViewHolder) convertView.getTag();
        }
        holder.name.setText(items.get(position).get("username"));
        holder.email.setText(items.get(position).get("password"));
        holder.mobile.setText(items.get(position).get("address"));
       // Glide.with(c).load(items.get(position).get("imagevalues")).into(holder.imageView);

        return convertView;
    }
    static class ViewHolder{
        TextView name,email,mobile;
        //ImageView imageView;
    }
}
