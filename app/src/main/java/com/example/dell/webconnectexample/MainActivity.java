package com.example.dell.webconnectexample;

import android.app.Dialog;
import android.app.ProgressDialog;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private String TAG = MainActivity.class.getSimpleName();

    private ProgressDialog pDialog;
    private ListView lv;
    SwipeRefreshLayout swipe;
    ItemListAdapter adapter;
    static String ipAddress = "192.168.100.12";//192.168.100.12
    private static String folder = "android";

    // selectUrl to get contacts JSON
    private static String selectUrl = "http://" + ipAddress + "/"+ folder +"/selectdata.php";
    private static String addUrl = "http://" + ipAddress + "/"+ folder +"/insertdata.php";
    private static String updateUrl = "http://" + ipAddress + "/"+ folder +"/updatedata.php";
    private static String deleteUrl = "http://" + ipAddress + "/"+folder+"/deletedata.php";
    private static String imageurl = "http://" + ipAddress + "/imagefolder/";
    ArrayList<HashMap<String, String>> contactList;
    JSONArray jsonArray;
    JSONObject jsonObject;
    // Defining the Volley request queue that handles the selectUrl request concurrently
    RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        contactList = new ArrayList<>();

        lv = (ListView) findViewById(R.id.list);
        swipe = (SwipeRefreshLayout) findViewById(R.id.swipe);
        adapter = new ItemListAdapter(MainActivity.this, contactList);
        lv.setAdapter(new ItemListAdapter(MainActivity.this, contactList));
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                contactList.clear();
                getJsonValue();
            }
        });
        swipe.post(new Runnable() {
            @Override
            public void run() {
                swipe.setRefreshing(true);
                getJsonValue();
            }
        });

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent newIntent = new Intent(MainActivity.this, Inner.class);
                newIntent.putExtra("position", position);
                newIntent.putExtra("jsonarray", jsonArray.toString());
                startActivity(newIntent);
            }
        });
        //getJsonValue();
    }

    public void getJsonValue() {

        swipe.setRefreshing(true);
        requestQueue = Volley.newRequestQueue(MainActivity.this);
        StringRequest strReq = new StringRequest(Request.Method.GET,
                selectUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Object json = null;
                try {
                    json = new JSONTokener(response).nextValue();

                    if (json instanceof JSONArray) {
                        jsonArray = (JSONArray) json;
                        Log.i("jsonArray", "" + jsonArray);
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject c = jsonArray.getJSONObject(i);

                            String id = c.getString("id");
                            String username = c.getString("username");
                            String password = c.getString("password");
                            String address = c.getString("address");
                            //String imaage=c.getString("imagevalues");

                            // tmp hash map for single contact
                            HashMap<String, String> user = new HashMap<>();

                            // adding each child node to HashMap key => value
                            user.put("id", id);
                            user.put("username", username);
                            user.put("password", password);
                            user.put("address", address);
                            //user.put("imagevalues",imageurl+imaage);

                            // adding contact to contact list
                            contactList.add(user);
                            // adapter.notifyDataSetChanged();
                        }

                    }
                    // Check for error node in json
                } catch (JSONException e) {
                    // JSON error
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
                adapter.notifyDataSetChanged();
                swipe.setRefreshing(false);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Volley Error has occurred " + error, Toast.LENGTH_SHORT).show();
                Log.i("VOlleyError", "" + error);
                swipe.setRefreshing(false);
            }
        });
        // Adding request to request queue
        requestQueue.add(strReq);
    }


    public void addUser(View view) {
        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.setContentView(R.layout.add_dialog);
        dialog.setCancelable(false);
        dialog.setTitle("Enter Users Details : ");
        final EditText edtAddUser = (EditText) dialog.findViewById(R.id.edtAddUsername);
        final EditText edtAddPassword = (EditText) dialog.findViewById(R.id.edtAddPassword);
        final EditText edtAddUserAddress = (EditText) dialog.findViewById(R.id.edtAddUserAdress);

        Button btnAddUser = (Button) dialog.findViewById(R.id.btnDialogAdd);
        Button btnCancel = (Button) dialog.findViewById(R.id.btnDialogCancel);
        dialog.show();
        btnAddUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pDialog = new ProgressDialog(MainActivity.this);
                pDialog.setMessage("Please wait...");
                pDialog.setCancelable(false);
                pDialog.show();

                requestQueue = Volley.newRequestQueue(MainActivity.this);
                StringRequest strReq = new StringRequest(Request.Method.POST,
                        addUrl, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("responseinsert", "" + response);
                        pDialog.dismiss();
                        Object json = null;
                        try {
                            json = new JSONTokener(response).nextValue();
                            if (json instanceof JSONObject) {
                                jsonObject = (JSONObject) json;
                                String status = jsonObject.getString("status");
                                String status_message = jsonObject.getString("status_message");
                                if (status.equals("1")) {
                                    Toast.makeText(getApplicationContext(), "Data Inserted SuccessFully " + status_message, Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(getApplicationContext(), "Data Cannot be inserted " + status_message, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        //adapter.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "Volley Error has occurred " + error, Toast.LENGTH_SHORT).show();
                        Log.i("My error", "" + error);
                        pDialog.dismiss();
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> map = new HashMap<String, String>();
                        map.put("username", edtAddUser.getText().toString());
                        map.put("password", edtAddPassword.getText().toString());
                        map.put("address", edtAddUserAddress.getText().toString());
                        return map;
                    }
                };
                // Adding request to request queue
                requestQueue.add(strReq);
                dialog.dismiss();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    public void updateUser(View view) {
        final Dialog updateDialog = new Dialog(MainActivity.this);
        updateDialog.setContentView(R.layout.update_dialog);
        updateDialog.setTitle("Enter details to update: ");
        updateDialog.setCancelable(false);
        final EditText updateId = (EditText) updateDialog.findViewById(R.id.edt_update_id);
        final EditText updateUserName = (EditText) updateDialog.findViewById(R.id.edt_update_username);
        final EditText updatePassword = (EditText) updateDialog.findViewById(R.id.edt_update_password);
        final EditText updateAddress = (EditText) updateDialog.findViewById(R.id.edt_update_address);
        Button btnUpdate = (Button) updateDialog.findViewById(R.id.btn_update);
        Button btnCancel = (Button) updateDialog.findViewById(R.id.btn_cancel);
        updateDialog.show();

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pDialog = new ProgressDialog(MainActivity.this);
                pDialog.setMessage("Please wait...");
                pDialog.setCancelable(false);
                pDialog.show();

                requestQueue = Volley.newRequestQueue(MainActivity.this);
                StringRequest strReq = new StringRequest(Request.Method.POST,
                        updateUrl, new Response.Listener<String>() {
                    /**
                     * Catches the responses send by the server in response variable
                     * @param response
                     */
                    @Override
                    public void onResponse(String response) {
                        Log.i("responseupdate", "" + response);
                        pDialog.dismiss();
                        Object json = null;
                        try {
                            json = new JSONTokener(response).nextValue();
                            if (json instanceof JSONObject) {
                                jsonObject = (JSONObject) json;
                                String status = jsonObject.getString("status");
                                String status_message = jsonObject.getString("status_message");
                                if (status.equals("1")) {
                                    Toast.makeText(getApplicationContext(), "Data updated SuccessFully " + status_message, Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(getApplicationContext(), "Data Cannot be updated " + status_message, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        // adapter.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "Volley Error has occurred " + error, Toast.LENGTH_SHORT).show();
                        Log.i("My error", "" + error);
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> map = new HashMap<String, String>();
                        map.put("id", updateId.getText().toString());
                        map.put("username", updateUserName.getText().toString());
                        map.put("password", updatePassword.getText().toString());
                        map.put("address", updateAddress.getText().toString());
                        return map;
                    }
                };
                // Adding request to request queue
                requestQueue.add(strReq);

                updateDialog.dismiss();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateDialog.dismiss();
            }
        });

    }

    public void deleteUser(View view) {
        final Dialog deleteDialog = new Dialog(MainActivity.this);
        deleteDialog.setContentView(R.layout.delete_dialog);
        deleteDialog.setTitle("Enter id of data to delete user: ");
        deleteDialog.setCancelable(false);
        final EditText deleteId = (EditText) deleteDialog.findViewById(R.id.edt_delete_id);
        Button btnDelete = (Button) deleteDialog.findViewById(R.id.btn_delete1);
        Button btnCancel1 = (Button) deleteDialog.findViewById(R.id.btn_delete_cancel);
        deleteDialog.show();

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pDialog = new ProgressDialog(MainActivity.this);
                pDialog.setMessage("Please wait...");
                pDialog.setCancelable(false);
                pDialog.show();

                requestQueue = Volley.newRequestQueue(MainActivity.this);
                StringRequest strReq = new StringRequest(Request.Method.POST,
                        deleteUrl, new Response.Listener<String>() {
                    /**
                     * Catches the responses send by the server in response variable
                     * @param response
                     */
                    @Override
                    public void onResponse(String response) {
                        Log.i("responsedelete", "" + response);
                        pDialog.dismiss();
                        Object json = null;
                        try {
                            json = new JSONTokener(response).nextValue();
                            if (json instanceof JSONObject) {
                                jsonObject = (JSONObject) json;
                                String status = jsonObject.getString("status");
                                String status_message = jsonObject.getString("status_message");
                                if (status.equals("1")) {
                                    Toast.makeText(getApplicationContext(), "Data deleted SuccessFully " + status_message, Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(getApplicationContext(), "Data Cannot be deleted " + status_message, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        //adapter.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "Volley Error has occurred " + error, Toast.LENGTH_SHORT).show();
                        Log.i("My error", "" + error);
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> map = new HashMap<String, String>();
                        map.put("id", deleteId.getText().toString());
                        return map;
                    }
                };
                // Adding request to request queue
                requestQueue.add(strReq);

                deleteDialog.dismiss();
            }
        });
        btnCancel1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteDialog.dismiss();
            }
        });
    }
}
